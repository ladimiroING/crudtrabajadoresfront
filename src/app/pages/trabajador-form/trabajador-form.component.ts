import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Pais } from 'src/app/models/pais';
import { Trabajador } from 'src/app/models/trabajador';
import { PaisService } from 'src/app/services/pais.service';
import { TrabajadorService } from 'src/app/services/trabajador.service';

@Component({
  selector: 'app-trabajador-form',
  templateUrl: './trabajador-form.component.html',
  styleUrls: ['./trabajador-form.component.css']
})
export class TrabajadorFormComponent implements OnInit {

  
  trabajador:Trabajador=new Trabajador();
  paises:any;
  accion = 'Formulario de Registro';

  constructor(              
              private paisSvc:PaisService,
              private trabajadorSvc:TrabajadorService,
              private toastr: ToastrService,
              private aRoute: ActivatedRoute,
              private route: Router) 
  {  }

  ngOnInit(): void {
    
    this.paisSvc.listarPais().subscribe(res=>{
      this.paises=res;
    })
    this.aRoute.params.subscribe(params=>{
      let id:number = params['id'];
      if(id){
        this.accion = 'Formulario de Actualizacion';
        this.trabajadorSvc.trabajadorId(id).subscribe(data=>{
          this.trabajador=data;
        })
      }
    })
    
        
  }

  crearTrabajador(){
    this.trabajadorSvc.nuevoTrabajador(this.trabajador).subscribe(()=>{
      console.log(this.trabajador);
      this.toastr.info('Trabajador creado con exito!','Trabajador registrado')
        this.route.navigate(['/']);
      },err=>{
        console.log(err)
    })
  }

  actualizarTrabajador(){
    this.trabajadorSvc.editarTrabajador(this.trabajador).subscribe(data=>{
      console.log('Trabajador Actualizado!');
      this.toastr.success('Trabajador actualizado con exito!','Trabajador actualizado')
        this.route.navigate(['/']);
      },err=>{
        console.log(err)
    })
  }

  compararPaises(p1: Pais, p2:Pais):boolean{
    if(p1 === undefined && p2 === undefined) return true;
    return p1 === null || p2 === null || p1 === undefined || p2 === undefined ? false: p1.idpais == p2.idpais;
  }
}
