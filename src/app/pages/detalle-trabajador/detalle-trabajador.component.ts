import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Trabajador } from 'src/app/models/trabajador';
import { PaisService } from 'src/app/services/pais.service';
import { TrabajadorService } from 'src/app/services/trabajador.service';

@Component({
  selector: 'app-detalle-trabajador',
  templateUrl: './detalle-trabajador.component.html',
  styleUrls: ['./detalle-trabajador.component.css']
})
export class DetalleTrabajadorComponent implements OnInit {


  trabajador:Trabajador=new Trabajador();
  paises:any;
  
  constructor(
    private paisSvc:PaisService,
    private trabajadorSvc:TrabajadorService,
    private aRoute: ActivatedRoute,    
  ) 
  { }

  ngOnInit(): void {
  
    this.paisSvc.listarPais().subscribe(res=>{
      this.paises=res;
    })
    this.aRoute.params.subscribe(params=>{
      let id:number = params['id'];
      if(id){
        this.trabajadorSvc.trabajadorId(id).subscribe(data=>{
          this.trabajador=data;
        })
      }
    })
  
  }

}
