import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Pais } from 'src/app/models/pais';
import { Trabajador } from 'src/app/models/trabajador';
import { PaisService } from 'src/app/services/pais.service';
import { TrabajadorService } from 'src/app/services/trabajador.service';


@Component({
  selector: 'app-listarTrabajador',
  templateUrl: './listarTrabajador.component.html',
  styleUrls: ['./listarTrabajador.component.css']
})
export class listarTrabajadorComponent implements OnInit {

  trabajadores:any;
  paises:Pais[]=[];
  
  constructor(private trabajadorSvc: TrabajadorService,
              private toastr:ToastrService,
              private paisSvc:PaisService) 
  { }

  ngOnInit(): void {
    this.listarTrabajadores();
      
  }

  listarTrabajadores(){
    this.trabajadorSvc.listarTrabajador().subscribe(data=>{
      console.log(data)
      this.trabajadores=data;
    },err=>{
      console.log(err);
    })
  }

  eliminarTrabajador(id:any){
    console.log(id);
    this.trabajadorSvc.eliminarTrabajador(id).subscribe(data=>{
      this.listarTrabajadores();
      this.toastr.error('El trabajador fue eliminado!','Registro eliminado');
    },err=>{
      console.log(err);
    })
  }

}
