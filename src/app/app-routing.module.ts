import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetalleTrabajadorComponent } from './pages/detalle-trabajador/detalle-trabajador.component';
import { listarTrabajadorComponent } from './pages/listarTrabajador/listarTrabajador.component';
import { TrabajadorFormComponent } from './pages/trabajador-form/trabajador-form.component';

const routes: Routes = [
  {path:'',component:listarTrabajadorComponent},
  {path:'crearTrabajador',component:TrabajadorFormComponent},
  {path:'editarTrabajador/:id',component:TrabajadorFormComponent},
  {path:'detalleTrabajador/:id',component:DetalleTrabajadorComponent},
  {path:'**',redirectTo:'/',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
