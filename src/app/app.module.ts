import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { listarTrabajadorComponent } from './pages/listarTrabajador/listarTrabajador.component';
import { TrabajadorFormComponent } from './pages/trabajador-form/trabajador-form.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { DetalleTrabajadorComponent } from './pages/detalle-trabajador/detalle-trabajador.component';



@NgModule({
  declarations: [
    AppComponent,
    listarTrabajadorComponent,
    TrabajadorFormComponent,
    DetalleTrabajadorComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
