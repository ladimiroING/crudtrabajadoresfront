import { Pais } from "./pais";

export class Trabajador{
    idtrabajador?: number;
	nombres?: string;
	apellidos?: string;
	edad?: number;
	pais?:Pais;
}