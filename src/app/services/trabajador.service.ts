import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Trabajador } from '../models/trabajador';

@Injectable({
  providedIn: 'root'
})
export class TrabajadorService {

  private url: string="/api/trabajador"

  constructor(private http:HttpClient) { }

  listarTrabajador():Observable<any>{
    return this.http.get(this.url);
  }

  eliminarTrabajador(id:number):Observable<any>{
    return this.http.delete(this.url+'/'+id);
  }

  editarTrabajador(trabajador: Trabajador):Observable<any>{
    return this.http.put(this.url+'/actualizar/'+trabajador.idtrabajador,trabajador);
  }

  nuevoTrabajador(trabajador:Trabajador):Observable<Trabajador>{
    return this.http.post<Trabajador>(this.url+'/crear',trabajador);
  }

  trabajadorId(id:number):Observable<any>{
    return this.http.get(this.url+'/'+id);
  }

}
