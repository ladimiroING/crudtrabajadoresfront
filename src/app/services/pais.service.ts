import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pais } from '../models/pais';

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  private url='/api/pais';

  constructor(private http:HttpClient) { }

  listarPais():Observable<Pais[]>{
    return this.http.get<Pais[]>(this.url);
  }

}
